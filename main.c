/*
 * main.c
 *
 * device driver test tool for Haptics devices
 *
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *
*/

#include <sys/cdefs.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdarg.h>
#include <time.h>
#include <math.h>
#include <pthread.h>
#include <fcntl.h>
#include "haptics.h"

const char *haptic_devices[] = {
		"/dev/drvxxxx",
		"/dev/drv2624",
		"/dev/drv2625",
	};
	
const char *target_str[] = {
		"DRVXXXX",
		"DRV2624",
		"DRV2625",
	};
	
const char *MainLoopStr[]={
	"No Loop",
	"Loop Once",
	"Loop Twice",
	"Loop 3 times",
	"Loop 4 times",
	"Loop 5 times",
	"Loop 6 times",
	"Loop Infinitely"
};

const char *MainIntervalStr[]={
	"5 ms",
	"1 ms"
};

const char *MainScaleStr[]={
	"100%",
	"75%",
	"50%",
	"25%"
};

const char *TS2200EffectNameStr[]={
	"1 Strong Click - 100%", 					
	"2 Strong Click - 60%", 					
	"3 Strong Click - 30%", 					
	"4 Sharp Click - 100%", 					
	"5 Sharp Click - 60%", 					
	"6 Sharp Click - 30%", 					
	"7 Soft Bump - 100%", 						
	"8 Soft Bump - 60%", 						
	"9 Soft Bump - 30%", 						
	"10 Double Click - 100%", 					
	"11 Double Click - 60%", 					
	"12 Triple Click - 100%", 					
	"13 Soft Fuzz - 60%", 						
	"14 Strong Buzz - 100%", 					
	"15 750 ms Alert 100%", 					
	"16 1000 ms Alert 100%", 					
	"17 Strong Click 1 - 100%", 				
	"18 Strong Click 2 - 80%", 				
	"19 Strong Click 3 - 60%", 				
	"20 Strong Click 4 - 30%", 				
	"21 Medium Click 1 - 100%", 				
	"22 Medium Click 2 - 80%", 				
	"23 Medium Click 3 - 60%", 				
	"24 Sharp Tick 1 - 100%", 					
	"25 Sharp Tick 2 - 80%", 					
	"26 Sharp Tick 3 – 60%", 					
	"27 Short Double Click Strong 1 – 100%", 	
	"28 Short Double Click Strong 2 – 80%", 	
	"29 Short Double Click Strong 3 – 60%", 	
	"30 Short Double Click Strong 4 – 30%", 	
	"31 Short Double Click Medium 1 – 100%", 	
	"32 Short Double Click Medium 2 – 80%", 	
	"33 Short Double Click Medium 3 – 60%", 	
	"34 Short Double Sharp Tick 1 – 100%", 	
	"35 Short Double Sharp Tick 2 – 80%", 		
	"36 Short Double Sharp Tick 3 – 60%", 		
	"37 Long Double Sharp Click Strong 1 – 100%", 		
	"38 Long Double Sharp Click Strong 2 – 80%", 		
	"39 Long Double Sharp Click Strong 3 – 60%", 		
	"40 Long Double Sharp Click Strong 4 – 30%", 		
	"41 Long Double Sharp Click Medium 1 – 100%", 		
	"42 Long Double Sharp Click Medium 2 – 80%", 		
	"43 Long Double Sharp Click Medium 3 – 60%", 		
	"44 Long Double Sharp Tick 1 – 100%", 				
	"45 Long Double Sharp Tick 2 – 80%", 				
	"46 Long Double Sharp Tick 3 – 60%", 				
	"47 Buzz 1 – 100%", 								
	"48 Buzz 2 – 80%", 								
	"49 Buzz 3 – 60%", 								
	"50 Buzz 4 – 40%", 								
	"51 Buzz 5 – 20%", 								
	"52 Pulsing Strong 1 – 100%", 						
	"53 Pulsing Strong 2 – 60%", 						
	"54 Pulsing Medium 1 – 100%", 						
	"55 Pulsing Medium 2 – 60%", 						
	"56 Pulsing Sharp 1 – 100%", 						
	"57 Pulsing Sharp 2 – 60%", 						
	"58 Transition Click 1 – 100%", 					
	"59 Transition Click 2 – 80%", 					
	"60 Transition Click 3 – 60%", 					
	"61 Transition Click 4 – 40%", 					
	"62 Transition Click 5 – 20%", 					
	"63 Transition Click 6 – 10%", 					
	"64 Transition Hum 1 – 100%", 						
	"65 Transition Hum 2 – 80%", 						
	"66 Transition Hum 3 – 60%", 						
	"67 Transition Hum 4 – 40%", 						
	"68 Transition Hum 5 – 20%", 						
	"69 Transition Hum 6 – 10%", 						
	"70 Transition Ramp Down Long Smooth 1 – 100 to 0%",		
	"71 Transition Ramp Down Long Smooth 2 – 100 to 0%",		
	"72 Transition Ramp Down Medium Smooth 1 – 100 to 0%",		
	"73 Transition Ramp Down Medium Smooth 2 – 100 to 0%",		
	"74 Transition Ramp Down Short Smooth 1 – 100 to 0%",		
	"75 Transition Ramp Down Short Smooth 2 – 100 to 0%",		
	"76 Transition Ramp Down Long Sharp 1 – 100 to 0%",		
	"77 Transition Ramp Down Long Sharp 2 – 100 to 0%",		
	"78 Transition Ramp Down Medium Sharp 1 – 100 to 0%",		
	"79 Transition Ramp Down Medium Sharp 2 – 100 to 0%",		
	"80 Transition Ramp Down Short Sharp 1 – 100 to 0%",	
	"81 Transition Ramp Down Short Sharp 2 – 100 to 0%",	
	"82 Transition Ramp Up Long Smooth 1 – 0 to 100%", 
	"83 Transition Ramp Up Long Smooth 2 – 0 to 100%", 		
	"84 Transition Ramp Up Medium Smooth 1 – 0 to 100%",
	"85 Transition Ramp Up Medium Smooth 2 – 0 to 100%",
	"86 Transition Ramp Up Short Smooth 1 – 0 to 100%",
	"87 Transition Ramp Up Short Smooth 2 – 0 to 100%",
	"88 Transition Ramp Up Long Sharp 1 – 0 to 100%",
	"89 Transition Ramp Up Long Sharp 2 – 0 to 100%",
	"90 Transition Ramp Up Medium Sharp 1 – 0 to 100%",
	"91 Transition Ramp Up Medium Sharp 2 – 0 to 100%",
	"92 Transition Ramp Up Short Sharp 1 – 0 to 100%",
	"93 Transition Ramp Up Short Sharp 2 – 0 to 100%",
	"94 Transition Ramp Down Long Smooth 1 – 50 to 0%",
	"95 Transition Ramp Down Long Smooth 2 – 50 to 0%",
	"96 Transition Ramp Down Medium Smooth 1 – 50 to 0%",
	"97 Transition Ramp Down Medium Smooth 2 – 50 to 0%",
	"98 Transition Ramp Down Short Smooth 1 – 50 to 0%",
	"99 Transition Ramp Down Short Smooth 2 – 50 to 0%",
	"100 Transition Ramp Down Long Sharp 1 – 50 to 0%",
	"101 Transition Ramp Down Long Sharp 2 – 50 to 0%",
	"102 Transition Ramp Down Medium Sharp 1 – 50 to 0%",
	"103 Transition Ramp Down Medium Sharp 2 – 50 to 0%",
	"104 Transition Ramp Down Short Sharp 1 – 50 to 0%",
	"105 Transition Ramp Down Short Sharp 2 – 50 to 0%",
	"106 Transition Ramp Up Long Smooth 1 – 0 to 50%",
	"107 Transition Ramp Up Long Smooth 2 – 0 to 50%",
	"108 Transition Ramp Up Medium Smooth 1 – 0 to 50%",
	"109 Transition Ramp Up Medium Smooth 2 – 0 to 50%",
	"110 Transition Ramp Up Short Smooth 1 – 0 to 50%",
	"111 Transition Ramp Up Short Smooth 2 – 0 to 50%", 
	"112 Transition Ramp Up Long Sharp 1 – 0 to 50%",
	"113 Transition Ramp Up Long Sharp 2 – 0 to 50%",
	"114 Transition Ramp Up Medium Sharp 1 – 0 to 50%",
	"115 Transition Ramp Up Medium Sharp 2 – 0 to 50%",
	"116 Transition Ramp Up Short Sharp 1 – 0 to 50%",
	"117 Transition Ramp Up Short Sharp 2 – 0 to 50%",
	"118 Long buzz for programmatic stopping – 100%", 
	"119 Smooth Hum 1 (No kick or brake pulse) – 50%",
	"120 Smooth Hum 2 (No kick or brake pulse) – 40%",
	"121 Smooth Hum 3 (No kick or brake pulse) – 30%",
	"122 Smooth Hum 4 (No kick or brake pulse) – 20%",
	"123 Smooth Hum 5 (No kick or brake pulse) – 10%"
};
	
static void usage(enum Haptic_Target target)
{
	if (target == HAPTIC_DRV2625) {
		fprintf(stderr, "%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n",
			"usage: haptics -r Reg_Addr1 [count]                                (read, hexidecimal, U8)",
			"       haptics -w Reg_Addr Value1 Value2 ...                      (write, hexidecimal, U8)",
			"       haptics -s Reg_Addr Mask Value                           (set bit, hexidecimal, U8)",
			"       haptics -g                                                         (do diagnostics)",
			"       haptics -c                                                         (do calibration)",
			"       haptics -m [loop interval scale]                       (get/set main loop, decimal)",
			"       haptics -n [sequencer1 loop1 ...]                      (get/set sequencer, decimal)",
			"       haptics -p                                                (play waveform sequencer)",
			"       haptics -i [enable]                                               (enable more log)",
			"       haptics -x                                                          (stop playback)",
			HAPTICS_VERSION);
	} else if(target == HAPTIC_DRV2624) {
		fprintf(stderr, "%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n\%s\n%s\n%s\n",
			"usage: haptics -r Reg_Addr1 [count]                                (read, hexidecimal, U8)",
			"       haptics -w Reg_Addr Value1 Value2 ...                      (write, hexidecimal, U8)",
			"       haptics -s Reg_Addr Mask Value                           (set bit, hexidecimal, U8)",
			"       haptics -g                                                         (do diagnostics)",
			"       haptics -c                                                         (do calibration)",
			"       haptics -m [loop interval scale]                       (get/set main loop, decimal)",
			"       haptics -n [sequencer1 loop1 ...]                      (get/set sequencer, decimal)",
			"       haptics -p                                                (play waveform sequencer)",
			"       haptics -f                                                        (firmware reload)",
			"       haptics -i [enable]                                               (enable more log)",
			"       haptics -x                                                          (stop playback)",
			HAPTICS_VERSION);
	}
}

static int str2char(char *argv, unsigned char *pUInt8_Val){
	int str_len = strlen(argv), i, result = -1, j;
	unsigned char val[2] = {0};

	if(str_len > 2){
		fprintf(stderr, "invalid parameters\n");
		goto err;
	}

	for(i = (str_len-1), j=0; i >= 0; i--, j++){
		if((argv[i] <= '9')&&(argv[i] >= '0')){
			val[j] = argv[i] - 0x30;
		}else if((argv[i] <='f')&&(argv[i]>= 'a')){
			val[j] = argv[i] - 0x57;
		}else if((argv[i] <='F')&&(argv[i]>= 'A')){
			val[j] = argv[i] - 0x37;
		}else{
			fprintf(stderr, "reg/data out of range\n");
			goto err;
		}
	}
	
	*pUInt8_Val = (unsigned char)(val[0]|(val[1]<<4));
	result = 0;
	
err:
	return result;
}

static int str2decimal(char *argv, unsigned int *pUInt32_Val){
	int max_len = strlen(MAX_INT_STR), i, result = -1, j;
	int str_len = strlen(argv);
	unsigned int nValue = 0;
	unsigned char temp;

	if(str_len > max_len){
		fprintf(stderr, "invalid parameters\n");
		goto err;
	}
	
	for(i = (str_len-1), j=0; i >= 0; i--, j++){
		if((argv[i] <= '9')&&(argv[i] >= '0')){
			temp = argv[i] - 0x30;
			nValue += (temp * pow(10, j));
		}else{
			fprintf(stderr, "reg/data out of range\n");
			goto err;
		}
	}
	
	*pUInt32_Val = nValue;
	result = 0;
	
err:
	return result;
}
#if 0
static int str2short(char *argv, unsigned short *pUInt16_Val){
	int str_len = strlen(argv), i, j, result = -1;
	unsigned char val[4] = {0};

	if(str_len > 4){
		fprintf(stderr, "invalid parameters\n");
		goto err;
	}

	for(i = (str_len-1), j=0; i >= 0; i--, j++){
		if((argv[i] <= '9')&&(argv[i] >= '0')){
			val[j] = argv[i] - 0x30;
		}else if((argv[i] <='f')&&(argv[i]>= 'a')){
			val[j] = argv[i] - 0x57;
		}else if((argv[i] <='F')&&(argv[i]>= 'A')){
			val[j] = argv[i] - 0x37;
		}else{
			fprintf(stderr, "reg/data out of range\n");
			goto err;
		}
	}
	
	*pUInt16_Val = (unsigned short)(val[0]|(val[1]<<4)|(val[2]<<8)|(val[3]<<12));
	result = 0;

err:
	return result;
}
#endif

static int DRV262x_Reg_Write(int fileHandle, int argc, char **argv){
	int err = -1;
	unsigned char *pBuff = NULL, regaddr_UInt8, regval_UInt8;
	int i=0, reg_count = 0;
	
	if(argc < 4){
		fprintf(stderr, "invalid para numbers\n");
		goto err;
	}
	
	reg_count = argc - 3;

	pBuff = (unsigned char *)malloc(reg_count+2);
	if (pBuff == NULL) {
		fprintf(stderr, "not enough mem\n");
		goto err;
	}
	
	pBuff[0] = HAPTIC_CMDID_REG_WRITE;

	err = str2char(argv[2], &regaddr_UInt8);
	if(err < 0){
		goto err;
	}
	pBuff[1] = regaddr_UInt8;
		
	for(i=0; i< reg_count; i++){
		err = str2char(argv[i+3], &regval_UInt8);
		if(err < 0){
			goto err;
		}
		pBuff[i + 2] = regval_UInt8;
	}
	
	err = write(fileHandle, pBuff, reg_count+2);
	if(err != (reg_count+2)){
		fprintf(stderr, "write err=%d\n", err);
	}else{
		for(i=0; i< reg_count; i++){
			fprintf(stderr, "W Reg(0x%x)=0x%x\n", regaddr_UInt8+i, pBuff[2+i]);
		}
	}	

err:
	if(pBuff != NULL)
		free(pBuff);	
	
	return err;
}

static int DRV262x_Reg_Read(int fileHandle, int argc, char **argv){
	int err = 0;
	unsigned char pBuff[2], RegAddr_UInt8=0, RegCount_UInt8 = 1;
	int i=0;
	unsigned char *pRead_Buff = NULL;

	if(argc < 3){
		fprintf(stderr, "invalid para numbers\n");
		goto err;
	}
		
	pBuff[0] = HAPTIC_CMDID_REG_READ;
	
	err = str2char(argv[2], &RegAddr_UInt8);
	if(err < 0){
		goto err;
	}
	pBuff[1] = RegAddr_UInt8;

	err = write(fileHandle, pBuff, 2);
	if(err != 2){
		fprintf(stderr, "send read err=%d\n", err);
		goto err;
	}
	
	if(argc > 3){
		err = str2char(argv[3], &RegCount_UInt8);
		if(err < 0){
			goto err;
		}
	}
	
	pRead_Buff = (unsigned char *)malloc(RegCount_UInt8);
	if(pRead_Buff == NULL){
		fprintf(stderr, "not enough mem\n");
		goto err;		
	}
	
	err = read(fileHandle, pRead_Buff, RegCount_UInt8 );
	if(err != RegCount_UInt8){
		fprintf(stderr, "read back err=%d\n", err);		
	}else{
		for(i=0; i< RegCount_UInt8; i++){
			fprintf(stderr, "R Reg[0x%x] = 0x%x\n", RegAddr_UInt8+i, pRead_Buff[i]);					
		}
	}
	
err:
	if(pRead_Buff != NULL)
		free(pRead_Buff);	

	return err;
}

static int DRV262x_Reg_SetBit(int fileHandle, int argc, char **argv){
	int err = 0;
	unsigned char pBuff[4], regAddr_UInt8, regMask_UInt8, regVal_UInt8;

	if(argc != 5){
		fprintf(stderr, "invalid para numbers\n");
		goto err;
	}
	
	pBuff[0] = HAPTIC_CMDID_REG_SETBIT;
	
	err = str2char(argv[2], &regAddr_UInt8);
	if(err < 0){
		goto err;
	}
	pBuff[1] = regAddr_UInt8;
	
	err = str2char(argv[3], &regMask_UInt8);
	if(err < 0){
		goto err;
	}	
	pBuff[2] = regMask_UInt8;
	
	err = str2char(argv[4], &regVal_UInt8);
	if(err < 0){
		goto err;
	}
	pBuff[3] = regVal_UInt8;
	
	err = write(fileHandle, pBuff, 4);
	if(err != 4){
		fprintf(stderr, "setbit err=%d\n", err);
	}else{
		fprintf(stderr, "S Reg(0x%x) Msk(0x%x) Val=0x%x\n", pBuff[1], pBuff[2], pBuff[3]);	
	}
	
err:
	return err;
}

static int DRV262x_doCalibration(int fileHandle){
	int err = 0;
	unsigned char pBuff[4];
	int i=0;
	struct timeval t_start,t_end;
	long cost_time = 0, start, end;

	pBuff[0] = HAPTIC_CMDID_RUN_CALIBRATION;

	gettimeofday(&t_start, NULL); 
	start = ((long)t_start.tv_sec)*1000+(long)t_start.tv_usec/1000; 

	err = write(fileHandle, pBuff, 1);
	if(err != 1){
		fprintf(stderr, "doCalibration err=%d\n", err);
		goto err;
	}

	while (1) {
		usleep(100*1000);
		fprintf(stderr, "%d ms\n", (i+1)*100);
		err = read(fileHandle, pBuff, 4);
		if(err == 4){
			gettimeofday(&t_end, NULL); 
			end = ((long)t_end.tv_sec)*1000+(long)t_end.tv_usec/1000; 
			cost_time = end - start;
			fprintf(stderr, "calibration finish after %ld ms\n", cost_time);

			if ((pBuff[0]&0x80) == 0x00) {
				fprintf(stderr, "calibration pass\n");
				fprintf(stderr, "A_CAL_COMP=0x%x, A_CAL_BEMF=0x%x, BEMF_GAIN=%d\n",
						pBuff[1], pBuff[2], pBuff[3]);
			} else {
				fprintf(stderr, "calibration fail, status=0x%x\n", pBuff[0]);
			}
			break;
		}else{
			i++;
		}
	}
	
err:
	return err;
}

static int DRV262x_doDiagnostics(int fileHandle){
	int err = 0;
	unsigned char pBuff[3];
	int i=0;
	struct timeval t_start,t_end;
	long cost_time = 0, start, end;

	pBuff[0] = HAPTIC_CMDID_RUN_DIAG;

	gettimeofday(&t_start, NULL); 
	start = ((long)t_start.tv_sec)*1000+(long)t_start.tv_usec/1000; 

	err = write(fileHandle, pBuff, 1);
	if (err != 1) {
		fprintf(stderr, "doCalibration err=%d\n", err);
		goto err;
	}

	while(1) {
		fprintf(stderr, "%d ms\n", (i+1)*100);
		usleep(100*1000);

		err = read(fileHandle, pBuff, 3);
		if (err == 3) {
			gettimeofday(&t_end, NULL); 
			end = ((long)t_end.tv_sec)*1000+(long)t_end.tv_usec/1000; 
			cost_time = end - start; 
			fprintf(stderr, "Diagnostics finish after %ld ms\n", cost_time);

			if ((pBuff[0] & 0x80) == 0x00) {
				fprintf(stderr, "Diagnostics pass\n");
				fprintf(stderr, "DiagZ=0x%x, DiagK=0x%x\n", pBuff[1], pBuff[2]);
			} else
				fprintf(stderr, "Diagnostics fail, status=0x%x\n", pBuff[0]);

			break;
		} else
			i++;
	}

err:
	return err;
}

static int DRV262x_config_waveform(int fileHandle, int argc, char **argv){
	int ret = 0;
	unsigned int value = 0;
	struct drv2625_wave_setting setting;
	int len = 0;
	int buf_len = sizeof(struct drv2625_wave_setting) + 1;
	unsigned char *pBuff = NULL;

	if((argc != 5)&&(argc!=2)){
		fprintf(stderr, "invalid para numbers\n");
		goto err;
	}
	
	if(argc == 2){
		len = 1;
	}else{	
		ret = str2decimal(argv[2], &value);
		if(ret < 0){
			fprintf(stderr, "invalid para 2\n");
			goto err;		
		}
		
		setting.mnLoop = (unsigned char)(value&0x07);
		
		ret = str2decimal(argv[3], &value);
		if(ret < 0){
			fprintf(stderr, "invalid para 3\n");
			goto err;		
		}
		setting.mnInterval = (unsigned char)(value&0x01);
		
		ret = str2decimal(argv[4], &value);
		if(ret < 0){
			fprintf(stderr, "invalid para 4\n");
			goto err;		
		}
		setting.mnScale = (unsigned char)(value&0x03);
		
		len = buf_len;
	}
		
	pBuff = (unsigned char *)malloc(buf_len);
	if(pBuff == NULL){
		fprintf(stderr, "not enough mem\n");
		goto err;		
	}

	pBuff[0] = HAPTIC_CMDID_CONFIG_WAVEFORM;
	if(len > 1){
		memcpy(&pBuff[1], &setting, buf_len-1);
	}

	ret = write(fileHandle, pBuff, len);
	if(ret < 0){
		fprintf(stderr, "config waveform err=%d\n", ret);
	}else{
		if(len > 1){
			fprintf(stderr, "Config Waveform: Loop(%s) Interval(%s) Scale(%s)\n", 
				MainLoopStr[setting.mnLoop], 
				MainIntervalStr[setting.mnInterval], 
				MainScaleStr[setting.mnScale]);	
		}else{
			ret = read(fileHandle, pBuff, buf_len -1);
			if(ret < 0){
				fprintf(stderr, "get config err=%d\n", ret);
			}else{
				memcpy(&setting, pBuff, buf_len -1);
				fprintf(stderr, "Waveform Config: Loop(%d,%s) Interval(%d, %s) Scale(%d, %s)\n", 
					setting.mnLoop, MainLoopStr[setting.mnLoop], 
					setting.mnInterval, MainIntervalStr[setting.mnInterval], 
					setting.mnScale, MainScaleStr[setting.mnScale]);
			}
		}
	}
	
err:
	if(pBuff != NULL)
		free(pBuff);
	
	return ret;
}

static int DRV262x_set_sequencer(int fileHandle, enum Haptic_Target target, int argc, char **argv)
{
	int ret = 0, i;
	unsigned int value = 0;
	int count = 0;
	struct drv2625_waveform_sequencer sequencer;
	int add_tail = 0;
	int len = 0;
	int seq_size = sizeof(struct drv2625_waveform_sequencer);
	unsigned char *pBuff = NULL;

	if (argc % 2) {
		fprintf(stderr, "invalid para numbers\n");
		goto err;
	}

	memset(&sequencer, 0, seq_size);

	if(argc == 2){
		len = 1;
	}else{
		count = (argc-2)/2;
		if(count > DRV262x_SEQUENCER_SIZE){
			fprintf(stderr, "invalid para numbers\n");
			goto err;
		}

		for (i = 0; i < count; i++) {
			ret = str2decimal(argv[2+i*2], &value);
			if (ret < 0) {
				fprintf(stderr, "invalid para\n");
				goto err;
			}
			if ((value&0x7f) > ARRAY_LEN(TS2200EffectNameStr)) {
				fprintf(stderr, "invalid para\n");
				goto err;
			}
			sequencer.msWaveform[i].mnEffect = (unsigned char)value;
			if(sequencer.msWaveform[i].mnEffect == 0){
				add_tail = 1;
				break;
			}
			ret = str2decimal(argv[3+i*2], &value);
			if(ret < 0){
				fprintf(stderr, "invalid para\n");
				goto err;
			}
			sequencer.msWaveform[i].mnLoop = (unsigned char)(value&0x03);	
		}
		
		if((add_tail == 0)&&(count < DRV262x_SEQUENCER_SIZE)){
			sequencer.msWaveform[count].mnEffect = 0;
		}
		len = 1 + seq_size;
	}

	pBuff = (unsigned char *)malloc(1 + seq_size);
	if(pBuff == NULL){
		fprintf(stderr, "not enough mem\n");
		goto err;
	}

	pBuff[0] = HAPTIC_CMDID_SET_SEQUENCER;
	if (len > 1) {
		memcpy(&pBuff[1], &sequencer, seq_size);
	}

	ret = write(fileHandle, pBuff, len);
	if(ret != len){
		fprintf(stderr, "write error %d\n", ret);
		goto err;
	}

	if (len > 1) {
		fprintf(stderr, "Set Sequencer :\n");
		for (i = 0; i < count; i++) {
			if(sequencer.msWaveform[i].mnEffect == 0) {
				fprintf(stderr, "\tSequencer[%d]: end\n", i);
				break;
			} else if (sequencer.msWaveform[i].mnEffect < ARRAY_LEN(TS2200EffectNameStr)) {
				if (target == HAPTIC_DRV2625) {
					fprintf(stderr, "\tSequencer[%d]:Effect(%s) Loop(%s)\n", 
							i,
							TS2200EffectNameStr[sequencer.msWaveform[i].mnEffect -1],
							MainLoopStr[sequencer.msWaveform[i].mnLoop]);
				} else if (target == HAPTIC_DRV2624) {
					fprintf(stderr, "\tSequencer[%d]:Effect(%d) Loop(%s)\n", 
							i, 
							i+1,
							MainLoopStr[sequencer.msWaveform[i].mnLoop]);
				}
			} else if (sequencer.msWaveform[i].mnEffect&0x80){
				fprintf(stderr, "\tSequencer[%d]:Delay(%d ms) Loop(%s)\n", 
							i,
							(sequencer.msWaveform[i].mnEffect&0x7f)*10,
							MainLoopStr[sequencer.msWaveform[i].mnLoop]);
			}else{
				fprintf(stderr, "\tSequencer[%d]: error, effect id=%d\n", 
							i, 
							sequencer.msWaveform[i].mnEffect);	
			}
		}
	} else {
		ret = read(fileHandle, pBuff, seq_size);
		if(ret != seq_size){
			fprintf(stderr, "read sequence err=%d\n", ret);
			goto err;
		}else{
			memcpy(&sequencer, pBuff, seq_size);
			fprintf(stderr, "Get Sequencer :\n");
			
			for(i = 0; i < DRV262x_SEQUENCER_SIZE; i++)
			{
				if(sequencer.msWaveform[i].mnEffect == 0) {
					fprintf(stderr, "\tSequencer[%d]: end\n", i);
					break;
				} else if (sequencer.msWaveform[i].mnEffect < ARRAY_LEN(TS2200EffectNameStr)) {
					if (target == HAPTIC_DRV2625) {
						fprintf(stderr, "\tSequencer[%d]:Effect(%s) Loop(%s)\n", 
								i, 
								TS2200EffectNameStr[sequencer.msWaveform[i].mnEffect -1],
								MainLoopStr[sequencer.msWaveform[i].mnLoop]);
					} else if (target == HAPTIC_DRV2624) {
						fprintf(stderr, "\tSequencer[%d]:Effect(%d) Loop(%s)\n", 
								i, 
								i+1,
								MainLoopStr[sequencer.msWaveform[i].mnLoop]);
					}
				} else if (sequencer.msWaveform[i].mnEffect&0x80) {
					fprintf(stderr, "\tSequencer[%d]:Delay(%d ms) Loop(%s)\n", 
								i, 
								(sequencer.msWaveform[i].mnEffect&0x7f)*10,
								MainLoopStr[sequencer.msWaveform[i].mnLoop]);
				} else {
					fprintf(stderr, "\tSequencer[%d]: error, effect id=%d\n", 
								i, 
								sequencer.msWaveform[i].mnEffect);	
				}
			}
		}
	}

err:
	if(pBuff != NULL)
		free(pBuff);

	return ret;
}

static int DRV262x_play_sequencer(int fileHandle){
	int err = 0;
	unsigned char pBuff[3];

	pBuff[0] = HAPTIC_CMDID_PLAY_EFFECT_SEQUENCE;

	err = write(fileHandle, pBuff, 1);
	if(err != 1){
		fprintf(stderr, "Play Sequence err %d\n", err);
		goto err;
	}

	fprintf(stderr, "Play Sequence Start %d\n", err);
err:
	return err;
}

static int DRV262x_fw_reload(int fileHandle)
{
	int err = 0;
	unsigned char pBuff[3];

	pBuff[0] = HAPTIC_CMDID_UPDATE_FIRMWARE;

	err = write(fileHandle, pBuff, 1);
	if(err != 1){
		fprintf(stderr, "fw reload err %d\n", err);
		goto err;
	}

	fprintf(stderr, "fw reload triggered\n");

err:
	return err;
}

static int DRV262x_enable_log(int fileHandle, int argc, char **argv)
{
	int err = 0, len;
	unsigned char pBuff[4], nValue;

	pBuff[0] = HAPTIC_CMDID_REGLOG_ENABLE;
	len = 1;

	if(argc > 2) {
		err = str2char(argv[2], &nValue);
		if(err < 0){
			goto err;
		}
		pBuff[1] = nValue;
		len++;
	}

	err = write(fileHandle, pBuff, len);
	if(err != len){
		fprintf(stderr, "enable log error=%d\n", err);
		goto err;
	}

	if (len == 2) {
		if(pBuff[1])
			fprintf(stderr, "Enable Log\n");
		else
			fprintf(stderr, "Disable Log\n");
		goto err;
	}

	err = read(fileHandle, pBuff, 1);
	if (err != 1) {
		fprintf(stderr, "enable log read error=%d\n", err);
		goto err;
	}

	if(pBuff[0])
		fprintf(stderr, "Log Enabled\n");
	else
		fprintf(stderr, "Log Disabled\n");

err:
	return err;
}

static int DRV262x_stop(int fileHandle)
{
	int err = 0;
	unsigned char pBuff[3];

	pBuff[0] = HAPTIC_CMDID_STOP;

	err = write(fileHandle, pBuff, 1);
	if(err != 1){
		fprintf(stderr, "stop err %d\n", err);
		goto err;
	}

	fprintf(stderr, "Stop playback\n");

err:
	return err;
}

static enum Haptic_Target getDevTarget(int index){
	enum Haptic_Target target;
	switch(index){
		case 1:
			target = HAPTIC_DRV2624;
			break;
		case 2:
			target = HAPTIC_DRV2625;
			break;
		default:
			target = HAPTIC_DRVXXXX;
			break;
	}
	return target;
}

static int getDevHandle(enum Haptic_Target *pTarget){
	int fileHandle = -1, i;
    struct stat sbuf;
	int dev_count = ARRAY_LEN(haptic_devices);

	for(i=0; i< dev_count; i++){
		if (stat(haptic_devices[i], &sbuf) == 0){
			*pTarget = getDevTarget(i);
            break;
		}
	}
	
	if(i == dev_count){
		fprintf(stderr, "[ERROR]dev nodes not found\n");
	}else{
		fileHandle = open(haptic_devices[i], O_RDWR);
		if(fileHandle < 0 ){
			fprintf(stderr, "[ERROR]file(%s) open_RDWR error\n", haptic_devices[i]);
		}
	}

	return fileHandle;
}

int main(int argc, char **argv)
{
	int ret = 0;
	int ch;
	int fileHandle = -1;
	enum Haptic_Target target;

	fileHandle = getDevHandle(&target);
	if(fileHandle < 0 ){
		fprintf(stderr, " file handle err=%d\n", fileHandle);
		return ret;
	}
	
	if(argc == 1){
		usage(target);
		return 0;
	}

	while ((ch = getopt(argc, argv, "wrsgcmnpfix")) != -1) {
		switch (ch) {
		case 'w': 
			ret = DRV262x_Reg_Write(fileHandle, argc, argv);
			break;
		case 'r':
			ret = DRV262x_Reg_Read(fileHandle, argc, argv);
			break;
		case 's':
			ret = DRV262x_Reg_SetBit(fileHandle, argc, argv);
			break;
		case 'g':
			ret = DRV262x_doDiagnostics(fileHandle);
			break;
		case 'c':
			ret = DRV262x_doCalibration(fileHandle);
			break;
		case 'm':
			ret = DRV262x_config_waveform(fileHandle, argc, argv);
			break;
		case 'n':
			ret = DRV262x_set_sequencer(fileHandle, target, argc, argv);
			break;
		case 'p':
			ret = DRV262x_play_sequencer(fileHandle);
			break;
		case 'f':
			if (target != HAPTIC_DRV2624)
				usage(target);
			else
				ret = DRV262x_fw_reload(fileHandle);
			break;
		case 'i':
			ret = DRV262x_enable_log(fileHandle, argc, argv);
			break;
		case 'x':
			ret = DRV262x_stop(fileHandle);
			break;
		default:
			usage(target);
			break;
		}
	}

	if(fileHandle > 0 )
		close(fileHandle);
	
	return ret;
}
