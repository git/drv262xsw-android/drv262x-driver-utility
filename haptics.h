/*
 *	2016 @ Texas Intruments, Inc.
 *
*/
#include <fcntl.h>
#include <stdio.h>
#include <sys/ioctl.h>

#define	HAPTICS_VERSION		"Version : 3.01 (09th, March, 2017)"

#define	DRV262x_SEQUENCER_SIZE	8

enum Haptic_Target{
	HAPTIC_DRVXXXX,
	HAPTIC_DRV2624,
	HAPTIC_DRV2625,
};

struct drv2625_waveform {
	unsigned char mnEffect;
	unsigned char mnLoop;	
};

struct drv2625_waveform_sequencer {
	struct drv2625_waveform msWaveform[DRV262x_SEQUENCER_SIZE];
};

struct drv2625_wave_setting {
	unsigned char mnLoop;
	unsigned char mnInterval;
	unsigned char mnScale;	
};

#define	FIFO_PBK_BYTES_LEN			100
#define	DRV2604_RAM_SIZE			(2*1024)
#define	MAX_READ_BYTES				0xff

/* Commands */
#define HAPTIC_CMDID_PLAY_SINGLE_EFFECT     0x01
#define HAPTIC_CMDID_PLAY_EFFECT_SEQUENCE   0x02
#define HAPTIC_CMDID_PLAY_TIMED_EFFECT      0x03
#define HAPTIC_CMDID_GET_DEV_ID             0x04
#define HAPTIC_CMDID_RUN_DIAG               0x05
#define HAPTIC_CMDID_AUDIOHAPTIC_ENABLE     0x06
#define HAPTIC_CMDID_AUDIOHAPTIC_DISABLE    0x07
#define HAPTIC_CMDID_AUDIOHAPTIC_GETSTATUS  0x08

#define HAPTIC_CMDID_REG_WRITE				0x09
#define HAPTIC_CMDID_REG_READ				0x0a
#define HAPTIC_CMDID_REG_SETBIT				0x0b
#define HAPTIC_CMDID_PATTERN_RTP			0x0c
#define HAPTIC_CMDID_RTP_SEQUENCE			0x0d
#define HAPTIC_CMDID_PLAY_FIFO				0x0e
#define HAPTIC_CMDID_ANALOG_INPUT			0x0f
#define HAPTIC_CMDID_GET_EFFECT_COUNT		0x10
#define HAPTIC_CMDID_UPDATE_FIRMWARE		0x11
#define HAPTIC_CMDID_READ_FIRMWARE			0x12
#define HAPTIC_CMDID_RUN_CALIBRATION		0x13
#define	HAPTIC_CMDID_CONFIG_WAVEFORM	 	0x14
#define	HAPTIC_CMDID_SET_SEQUENCER 			0x15
#define	HAPTIC_CMDID_REGLOG_ENABLE			0x16

#define	HAPTIC_CMDID_STOP					0xFF

/* Command size */
#define HAPTIC_CMDSZ_SINGLE_EFFECT     2
#define HAPTIC_CMDSZ_EFFECT_SEQUENCE   9
#define HAPTIC_CMDSZ_TIMED_EFFECT      3
#define HAPTIC_CMDSZ_STOP              1

#define MAX_TIMEOUT 10000 /* 10s */
#define REGS_TOTAL 0x30

#define MAX_INT_STR	"4294967295"

#define	RTP_DURATION_MAX			10000 //ms

#define ARRAY_LEN(x) ((int)(sizeof(x)/sizeof((x)[0])))

#define	DRV2625_MAGIC_NUMBER	0x32363235	/* '2625' */

#define	DRV2625_CONFIG_WAVEFORM	 			_IOWR(DRV2625_MAGIC_NUMBER, 1, struct drv2625_wave_setting *)
#define	DRV2625_SET_SEQUENCER 				_IOWR(DRV2625_MAGIC_NUMBER, 2, struct drv2625_waveform_sequencer *)